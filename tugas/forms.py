from django import forms
from . import models

class Form_Jadwal(forms.Form):
	pilihan_kategori = [
		('Kuliah', 'Kuliah'),
		('Personal', 'Personal'),
		('Lainnya', 'Lainnya'),
	]
	nama_kegiatan = forms.CharField(label='Nama:', max_length=30, 
		widget=forms.TextInput())
	tempat_kegiatan = forms.CharField(label='Tempat:', max_length=30, 
		widget=forms.TextInput())
	hari_kegiatan = forms.CharField(label='Hari:', max_length=10,
		widget=forms.TextInput())
	tanggal_kegiatan = forms.DateField(label='Tanggal(dd-mm-yyyy):',
		widget=forms.SelectDateWidget())
	waktu_kegiatan = forms.CharField(label='Waktu(hh-mm)', max_length=10,
		widget=forms.TextInput())
	kategori_kegiatan = forms.ChoiceField(choices=pilihan_kategori, label='Kategori:')

	class Meta:
		model = models.Jadwal
		fields = ['nama', 'tempat', 'tanggal', 'waktu', 'kategori']