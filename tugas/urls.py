from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from .views import resume, home, form_jadwal, jadwal_kegiatan, delete_jadwal

urlpatterns = [
    path('', home, name='home'),
    path('resume/', resume, name='resume'),
    path('form-jadwal/', form_jadwal, name='Form Jadwal'),
    path('jadwal-kegiatan/', jadwal_kegiatan, name='Jadwal Kegiatan'),
    path('delete/', delete_jadwal, name='Delete'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
