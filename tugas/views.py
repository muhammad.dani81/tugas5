# Create your views here.

from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import models, forms

def home(request):
	return render(request,'index.html')

def resume(request):
    return render(request,'resume.html')

def form_jadwal(request):
	form = forms.Form_Jadwal()
	if request.method == 'POST':
		form = forms.Form_Jadwal(request.POST)
		if form.is_valid():
			jadwal = models.Jadwal(
				nama = form.cleaned_data['nama_kegiatan'],
				tempat = form.cleaned_data['tempat_kegiatan'],
				hari = form.cleaned_data['hari_kegiatan'],
				tanggal= form.cleaned_data['tanggal_kegiatan'],
				waktu = form.cleaned_data['waktu_kegiatan'],
				kategori = form.cleaned_data['kategori_kegiatan'],
			)
			jadwal.save()

	return render(request, 'form.html', {'form' : form})

def jadwal_kegiatan(request):
	list_jadwal = models.Jadwal.objects.all()
	return render(request, 'jadwal.html', {'jadwal' : list_jadwal})

def delete_jadwal(request):
	models.Jadwal.objects.all().delete()
	return redirect('../jadwal-kegiatan')