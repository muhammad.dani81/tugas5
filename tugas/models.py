from django.db import models

# Create your models here.
class Jadwal(models.Model):
	nama = models.CharField(max_length = 50)
	tempat = models.CharField(max_length = 50)
	hari = models.CharField(max_length=10)
	tanggal = models.DateField()
	waktu = models.CharField(max_length=10)
	kategori = models.CharField(max_length=1)


